from django.apps import AppConfig


class DimokaConfig(AppConfig):
    name = 'dimoka'

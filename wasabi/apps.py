from django.apps import AppConfig


class WasabiConfig(AppConfig):
    name = 'wasabi'
